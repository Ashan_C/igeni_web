import Register from "./components/Register";

export const routes = [
    {
        path: '/',
        name: 'sign-up',
        meta: {layout: 'sign-up'},
        component: Register
    },
];