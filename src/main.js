import Vue from 'vue'
import App from './App.vue'
import VueAxios from "vue-axios"
import axios from 'axios'
import VueRouter from "vue-router"
import VueSweetalert2 from "vue-sweetalert2"
import 'sweetalert2/dist/sweetalert2.min.css';
import VueAuth from '@websanova/vue-auth'
import auth from './auth'

import { routes } from './routes'

const router = new VueRouter({
  mode: 'history',
  routes
});


Vue.router = router

Vue.use(VueRouter)
Vue.use(VueAxios, axios)
Vue.use(VueSweetalert2)
Vue.use(VueAuth, auth)


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
